import './App.css';
import { useState } from 'react';
import RestrictedPage from "./components/RestrictedPage";

const App = () => {

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const user = "Renato";

  const Login = () => {
    setIsLoggedIn(true);
  }

  const Logout = () => {
    setIsLoggedIn(false);
  }

  return (
    <div className="App">
      <header className="App-header">
        <RestrictedPage isLoggedIn={isLoggedIn} user={user} Login={Login} Logout={Logout}/>        
      </header>
    </div>
  );
}

export default App;
