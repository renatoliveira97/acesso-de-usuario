import './style.css';

const RestrictedPage = ({isLoggedIn, user, Login, Logout}) => { 
    return (
        <>
            {isLoggedIn ? (
            <div>
                <p className="logado">Bem-vindo, {user}!</p>
                <button onClick={Logout}>Sair</button>
            </div>
            ) : (
            <div>
                <p>Você não pode acessar esta página</p>
                <button onClick={Login}>Entrar</button>
            </div>
            )}
        </>        
    );
}

export default RestrictedPage;